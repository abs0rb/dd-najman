import * as log from "./logging.js";

var players = [];

Hooks.on("ready", () => {
    let playerList = game.users.players;
    let playerNames = playerList.map(p => p.data.name);
    let playerIds = playerList.map(p => ({ "name": p.data.name, "playerId": p.data._id }));

    players = playerIds;

    let actors = game.actors
        .filter(a => playerNames.includes(a.data.name))
        .map(a => ({ "name": a.name, "actorId": a.id }));

    actors.forEach(actor => {
        let targetId = players.find(player => player.name == actor.name);
        let token = game.canvas.tokens.objects.children.find(c => c.data.actorId == actor.actorId);
        targetId.actorId = actor.actorId;
        targetId.tokenId = token.data._id;
        targetId.x = token.x;
        targetId.y = token.y;
    });
});

Hooks.on("updateToken", (scene, data, moved) => {
    let newX = data.x;
    let newY = data.y;
    let target = players.find(player => player.tokenId == data._id)

    if (newX != undefined) {
        target.x = newX;
    }

    if (newY != undefined) {
        target.y = newY;
    }

    if (game.users.current.isGM == true) {
        return;
    }

    let currentUserId = game.users.current.data._id;
    let currentPlayerCoords = players
        .filter(player => player.playerId == currentUserId)
        .map(p => ({ "x": p.x, "y": p.y }))[0];

    for (const item of Object.entries(game.webrtc.settings.client.users)) {
        let targetCoords = players.filter(player => player.playerId == item[0])
            .map(p => ({ "x": p.x, "y": p.y }))[0];

        if (item[0] != currentUserId) {
            let targetPlayerName = players
                .filter(p => p.playerId == item[0])
                .map(p => p.name);

            let targetVolume = globalThis.najman.getDistance(currentPlayerCoords, targetCoords);
            log.info("Volume:", targetVolume, "for player:", targetPlayerName[0]);
            item[1].volume = targetVolume;
        }
    }
});