import { LANG_NAME, MODULE_NAME } from "./lib/constants.js";
import * as log from "./lib/logging.js";

export default class Najman {
  constructor() {

  }

  getDistance(p0, p1) {
    var a = p0.x - p1.x;
    var b = p0.y - p1.y;

    let result = (1000 - Math.sqrt(a * a + b * b)) / 1000;

    if (result < 0) {
      result = 0;
    }

    return result;
  }
}